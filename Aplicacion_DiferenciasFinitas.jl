##ingreso e datos
alpha=1
dT=0.00025##estasson con respecto al tiempo
dX=0.05##este es el valor de h, ya que son las duvisiones en el eje x
n= 20
##creacion de vectores y matriz A
A= zeros((n-1),(n-1))
uJ_0= zeros((n-1),1)
uJ_1= zeros((n-1),1)
##calculo del valor de lambda
lambda= (alpha*(dT/(dX^2)))
x=zeros(1,(n-1))
G_xi=zeros((n-1),1)
for k in 1:(n-1)
    x[[k]]=[0+dX]
end
#calculo del vector u(i,0) tomando en cuenta u(i,0)= f(x) 
for i_0 in 1:(n-1)
    uJ_0[[i_0]]=[sin(pi*i_0*dX)]
end
#calculo de la matriz A
for i in 1:(n-1)
    if i<(n-1)
        A[[((n-1)*(i-1))+i]]=[-6]
    end
    if i<(n-1)
        A[[((n-1)*(i))+i]]=[4]
    end
    if i<(n-2)
        A[[((n-1)*(i))+i+2]]=[4]
    end
    if i<(n-2)
        A[[((n-1)*(i+1))+i]]=[-1]
    end
    if i<(n-4)
        A[[((n-1)*(i+1))+i+4]]=[-1]
    end
end
A[[1]]=[-5]
A[[(n-1)^2]]=[-5]
A[[2]]=[4]
A[[3]]=[-1]
A[[(n-1)+4]]=[-1]
print(A)
#calculo de u(i,1) 
for i_0 in 1:(n-1)
    G_xi[[i_0]]=[0]
end 
uJ_1=(A*uJ_0*(0.5)*lambda^2)+uJ_0+(dT*G_xi)
#iteraciones para u_n
iteraciones = 32000
t = dT*iteraciones
for m in 1:iteraciones
    u_n=(A*uJ_1*lambda^2)+(2*uJ_1)-uJ_0
    uJ_0=uJ_1
    uJ_1=u_n
end

print("valores en y")
print(uJ_1)

##grafica
y1 = uJ_1
x1 = range(0.05,stop=0.995,length=(n-1))
print("valores en x")
print(x1)
plot(x1,y1,title="Viga Vibrante",label="Numerica, t=$t")